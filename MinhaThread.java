public class MinhaThread extends Thread{
	private int threadId;

	public MinhaThread(int id)
	{
		this.threadId = id;
	}

	public void run()
	{
		int contador = 10;

		for(int i = 0; i< contador; i++)
			System.out.println("MinhaThread ["+ this.threadId +"] - contador"+ i);
	}
}